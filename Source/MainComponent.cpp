/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    textButton1.setButtonText("Click Me!");
//    addAndMakeVisible(&textButton1);
    textButton1.addListener(this);
    
    textButton2.setButtonText("But Don't forget me!");
//    addAndMakeVisible(&textButton2);
    textButton2.addListener(this);
    
    slider1.setSliderStyle(Slider::Rotary);
//    addAndMakeVisible(&slider1);
    slider1.addListener(this);
    
    comboBox1.addItem("FrankenSound", 1);
    comboBox1.addItem("The FreshMan", 2);
    comboBox1.addItem("The Rewinder", 3);
//    addAndMakeVisible(&comboBox1);
    comboBox1.addListener(this);
    
    textEditor1.setText("Edit Me!");
//    addAndMakeVisible(&textEditor1);
    
//    addAndMakeVisible(&myColourSelector);
    
    addAndMakeVisible(myNeilButton);
    
    addAndMakeVisible(myButtonRow);
    
    
    
    setSize (500, 400);
}



// Destructor!!
MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    {
//        textButton1.setBounds(10, 10, getWidth() - 20, 40);
//        textButton2.setBounds(10, 50, getWidth() - 20, 40);
//        
//        slider1.setBounds(10, 90, getWidth() - 20, 40);
//        comboBox1.setBounds(10,130,getWidth()-20,40);
//        textEditor1.setBounds (10, 170, getWidth()-20, 40);
//        myColourSelector.setBounds(0, 0, getWidth(), getHeight() / 2);
//        
//        myNeilButton.setBounds(50, 50, getWidth() / 2, getHeight()/2);
        
        myButtonRow.setBounds(0, 0, getWidth(), getHeight() / 2);
        
        
        DBG("Updated height is : " << getHeight() << " \n");
        DBG("Updated width is : " << getWidth() << " \n");
    }
    
}

void MainComponent::paint (Graphics& g)

{
    
    int halfWidth = getWidth()/2.0;
    int halfHeight = getHeight()/2.0;
    
    // setColour(Colours:: 'nameofColour') needs to be reset (like a colour picker tool) before any painting / drawing.
//    g.setColour(Colours::burlywood);
//    
//    g.fillRect (0, 0, halfWidth, halfHeight);
    
//
//    g.fillRoundedRectangle (halfWidth, halfHeight, halfWidth, halfHeight, 20);
//
//    g.drawHorizontalLine (halfHeight, 0, getWidth());
//
//    g.setColour(Colours::darkmagenta);
//
//    g.drawSingleLineText ("SDA!", halfWidth, halfHeight);
    
//    g.setColour(myColourSelector.getCurrentColour());
//
//    g.drawEllipse (mouseX - 50, mouseY - 50, getWidth() / 4, getHeight() / 4, 10);

}

void MainComponent::mouseUp (const MouseEvent& event)

{
    DBG("Mouse clicked! \n x Coordinate is : " << event.x << "\n y Coordinate is : " << event.y);
    mouseX = event.x;
    mouseY = event.y;
    
    repaint();

}



void MainComponent::buttonClicked(Button* button)
{
    if (button == &textButton1)
    {
         DBG("Button 1 Clicked!");
    }
    else if (button == &textButton2)
        
    {
        DBG("Button 2 (needy button) Clicked!");
    }
   
}

void  MainComponent:: sliderValueChanged(Slider* slider)

{
    if( slider == &slider1)
        
    {
        DBG("Slider Value" << slider1.getValue());
    }
}


void MainComponent::comboBoxChanged (ComboBox* comboBoxThatHasChanged)
{
    if (comboBoxThatHasChanged == &comboBox1)
        
    {
        if (comboBox1.getSelectedId() == 1)
        {
            DBG("The Frankensound: Mangle your sounds!!");
            textEditor1.setText("The Frankensound: Mangle your sounds!!");
        }
        
        else if (comboBox1.getSelectedId() == 2)
        {
            DBG("The FreshMan: Enjoy Blissful, intelligent harmonisation to your vox!");
            textEditor1.setText("The FreshMan: Enjoy Blissful, intelligent harmonisation to your vox!");
        }
        else if (comboBox1.getSelectedId() == 3)
        {
            DBG("The Rewinder: Experience the warm sounds of analog delay!");
            textEditor1.setText("The Rewinder: Experience the warm sounds of analog delay!");
        }
    }

}
