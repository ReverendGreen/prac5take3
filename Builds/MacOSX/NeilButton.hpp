//
//  NeilButton.hpp
//  JuceBasicWindow
//
//  Created by Neil McGuiness on 06/10/2016.
//
//

#ifndef NeilButton_hpp
#define NeilButton_hpp

#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"

class NeilButton : public Component

{
public:
    
    NeilButton();
    
    ~NeilButton();

    
    void resized() override;

    void paint (Graphics& g) override;
    
    void mouseEnter (const MouseEvent& event) override;
    
    void mouseExit (const MouseEvent& event) override;
    
    void mouseDown (const MouseEvent& event) override;
    
    void mouseUp (const MouseEvent& event) override;
    
    
    
private:
    
    Label buttonName;
    Colour colour;
    Colour textColour;
};
#endif /* NeilButton_hpp */
