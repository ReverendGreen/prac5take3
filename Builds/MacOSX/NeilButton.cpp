//
//  NeilButton.cpp
//  JuceBasicWindow
//
//  Created by Neil McGuiness on 06/10/2016.
//
//

#include "NeilButton.hpp"
NeilButton:: NeilButton()

{
    colour = Colours::crimson;
    textColour = Colours:: aquamarine;
    buttonName.setText("Neil Button", NotificationType::dontSendNotification);
    addAndMakeVisible(&buttonName);
}

NeilButton:: ~NeilButton()

{
    
}

void NeilButton::resized()

{
    buttonName.setBounds(0, 0, getWidth() , getHeight());
}

void NeilButton::paint(Graphics &g)

{
    g.setColour(colour);
//    g.fillEllipse(50, 100, getWidth() / 2, getHeight() / 2);
    g.fillRoundedRectangle(0, 0, getWidth(), getHeight(),2);
    
    g.setColour(textColour);
    
    buttonName.setColour(0, textColour);
    
}

//void NeilButton::buttonName::paint(Graphics &g)
//{
//    
//}
//    
//    g.setColour(Colours::aquamarine);
//    g.drawText("Neil Button", getWidth() - 100, getHeight() - 150, getWidth() / 4, getHeight() / 4, Justification::centred);


void NeilButton::mouseEnter (const MouseEvent& event)

{
    colour = Colours::cadetblue;
    repaint();
}

void NeilButton:: mouseExit (const MouseEvent& event)

{
    colour = Colours::crimson;
    repaint();
}

void NeilButton:: mouseDown (const MouseEvent& event)

{
    colour = Colours::cyan;
    textColour = Colours::crimson;
    repaint();
}

void NeilButton:: mouseUp (const MouseEvent& event)

{
    colour = Colours::cadetblue;
    textColour = Colours::aquamarine;
    repaint();
}

