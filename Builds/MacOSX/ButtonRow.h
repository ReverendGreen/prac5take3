//
//  ButtonRow.h
//  JuceBasicWindow
//
//  Created by Neil McGuiness on 13/10/2016.
//
//

#ifndef ButtonRow_h
#define ButtonRow_h


#include <stdio.h>
#include "../JuceLibraryCode/JuceHeader.h"
#include "NeilButton.hpp"
#define BUTTON_NUMS 5




class ButtonRow : public Component,
public Button::Listener


{
    
public:
    
    ButtonRow();
    
    ~ButtonRow();
    
    void resized () override;
    
    void paint (Graphics& g) override;
    
    void buttonClicked (Button*);
    
private:
    
    NeilButton buttons[BUTTON_NUMS];
    
    
    
    
    
};



#endif /* ButtonRow_h */
