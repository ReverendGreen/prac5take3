//
//  ButtonRow.cpp
//  JuceBasicWindow
//
//  Created by Neil McGuiness on 13/10/2016.
//
//

#include <stdio.h>
#include "ButtonRow.h"


ButtonRow::ButtonRow()

{
    for(int i = 0; i < BUTTON_NUMS; i ++)
        
    {
        addAndMakeVisible(buttons[i]);
    }
    addAndMakeVisible(buttons[0]);
    
}

ButtonRow::~ButtonRow()

{
    
}

void ButtonRow::resized ()
{
    for(int i = 0; i < BUTTON_NUMS; i ++)
        
    {
        buttons[i].setBounds(i * getWidth()/BUTTON_NUMS, 0, getWidth()/BUTTON_NUMS, getHeight());
    }
}

void ButtonRow::paint (Graphics& g)
{
    
}
